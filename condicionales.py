# Estructuras de control
numero = 5

if numero == 5:
    print('El numero es 5')
else:
    print('El numero NO es 5')

# if-else-if

if numero == 1:
    print('El numero es 1')
elif numero == 2:
    print('El numero es 2')
elif numero == 3:
    print('El numero es 3')
else:
    print('El numero NO es 1, 2 o 3')

valor = 10

mensaje = "es 10" if valor == 10 else "no es 10"
print(mensaje)
