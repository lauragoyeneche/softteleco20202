tablero = [
    ['1', '2', '3'],
    ['4', '5', '6'],
    ['7', '8', '9']
]

print('-' * (len(tablero)*4+1))
fila = 0
while fila < len(tablero):
    col = 0
    while col < len(tablero[fila]):
        print(f' | {tablero[fila][col]}', end='')
        col = col + 1
    print('|')
    print('-' * (len(tablero)*4+1))
    fila = fila + 1

print('-' * (len(tablero)*4+1))
for fila in tablero:
    for elemento in fila:
            print(f' | {elemento}', end='')
    print('|')
    print('-' * (len(tablero)*4+1))
