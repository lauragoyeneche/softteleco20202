# acceder a cada caracter
cadena = "universidad"
print(cadena[0]) #primer caracter
print(cadena[5])
print(cadena[-1]) #ultimo caracter
print(cadena[0:5]) #subcadena (no incluye el segundo parametro)
print(cadena[3:7])

#metodos de string
longitud= len(cadena)
print(longitud)

inicia = cadena.startswith('uni')
print(inicia) #true

termina= cadena.endswith('uni')
print(termina) #false

esta= 'si' in cadena
print(esta)

posicion = cadena.find('ni')
print(posicion)

nombre= 'catalina'
edad=19
formato= f'el estudiante {nombre} tiene {edad} años'
print(formato)

frase = 'la casa es roja'
print(frase.title()) #capitalizar primeras letras)

cantidad= frase.count('la')
print(f'la cantidad es {cantidad}')

resultado = frase.replace('a', '4')
print(resultado)

#mayusculas y minusculas

print(frase.upper())
print(frase.lower())

#alfanumericos

clave = 'abc123'
clave2 = 'abc'
clave3 = '123'

print(clave.isalnum())
print(clave2.isalpha())
print(clave3.isdecimal())

