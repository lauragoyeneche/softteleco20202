numero = 5
# comparacion de igualdad
a = numero == 5
print(a)

# comparacion de diferentes

b = numero != 5
print(b)

c = numero > 5
d = numero < 5
e = numero >= 5
f = numero <= 5

print(a, b, c, d, e)

# comparacion con and

f= numero > 5 and numero < 10
print(f)

g= numero > 5 or numero < 10
print(g)
