class Persona:
    nombre = ' '
    edad = 0

    #metodo de negocio
    def es_adulto(self):
        if self.edad >= 18:
            return True
        else:
            return False


p1 = Persona()

# acceso a los atributos
p1.nombre = 'Catalina'
p1.edad = 18

print(p1.nombre)
print(p1.edad)

print(f'{p1.nombre}, {p1.edad}')


class Curso:
    codigo = 0
    nombre = ' '

    def __init__(self, codigo, nombre='vacio'):
        self.codigo = codigo
        self.nombre = nombre
    def __str__(self):
        return f'codigo={self.codigo}, nombre ={self.nombre}'
print

c1 = Curso(100, 'programacion')
print(c1.codigo)
print(c1.nombre)
print(c1)

c2= Curso(100, 'programacion')
c2.codigo=200
c2.nombre= 'bases'

c4= Curso(codigo=500, nombre= 'Laura')
print(c4)
