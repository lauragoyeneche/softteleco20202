# Inicializacion
x = 0
# ciclo
while x < 5:
    # instruccion a repetir
    print(f'x = {x}')
    # instruccion de rompimiento
    x = x + 1

# ciclo entre 0 y 3
for numero in range(4):
    print(numero)

print("=" * 20)

# ciclo entre 0 y 3
for numero in range(4, 10):
    print(numero, end=' ')


