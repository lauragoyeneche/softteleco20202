# declaracion de lista
lista_vacia = []
numeros = [1, 2, 3, 4, 5]
print(numeros)
print(f'la longitud de la lista es {len(numeros)}')
print(numeros[0])

variada = [1, 3.4, 'cadena', 4, True]
print(type(variada[2]))

# concatenar listas
primeros = [1, 2, 3, 4]
ultimos = [5, 6, 7, 8]
todos = primeros + ultimos
print(todos)
# modificar valor
todos[0] = 10
print(todos)

# agregar elemto
todos.append(100)
print(todos)
