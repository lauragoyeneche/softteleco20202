from ui import (
    Inicio,
    dibujar_tablero,
    empezar_juego,
    posicion_actual,
    verificar_posicion,
    opcion_jugador,
    verificar_tablero,
    verificar_jugadas
)

op =1
while op != 0:
    Inicio()
    i=1
    jugadores= empezar_juego()
    tablero= ['#'] * 10
    while True:
        en_juego= verificar_tablero(tablero)
        while not en_juego:
            posicion= opcion_jugador(tablero)
            if i % 2 == 0:
                actual = jugadores[1]
            else:
                actual = jugadores[0]
            #en juego
            posicion_actual(tablero, actual, int(posicion))
            dibujar_tablero(tablero)
            i += 1
            if verificar_jugadas(tablero, actual):
                print(f'!!!El jugador {actual}', ' ha ganado!!!!')
                break
            break



