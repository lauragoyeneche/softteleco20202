def Inicio():
    print("¡Bienvenidos al juego!")
tablero0 = [
    ['1', '2', '3'],
    ['4', '5', '6'],
    ['7', '8', '9']
]
print('-' * (len(tablero0)*4+1))
for fila in tablero0:
    for elemento in fila:
            print(f' | {elemento}', end='')
    print('|')
    print('-' * (len(tablero0)*4+1))


def dibujar_tablero(tablero):
    tablero_1 = """
___________________
|  1  |  2  |  3  |
|-----------------|
|  4  |  5  |  6  |
|-----------------|
|  7  |  8  |  9  |
|-----------------|
"""
    for i in range(1, 10):
        if (tablero[i] == 'O' or tablero[i] == 'X'):
            tablero_1 = tablero_1.replace(str(i), tablero[i])
        else:
            tablero_1 = tablero_1.replace(str(i), ' ')
    print(tablero_1)

def empezar_juego():
    jugador_uno = input("Seleccione 'X' o 'O'\n")
    while True:
        if jugador_uno.upper() == 'X':
            jugador2 = 'O'
            print("Usted ha escogido " + jugador_uno + ". El jugador 2 es " + jugador2)
            return jugador_uno.upper(), jugador2
        elif jugador_uno.upper() == 'O':
            jugador2 = 'X'
            print("Usted ha escogido " + jugador_uno + ". El jugador 2 es  " + jugador2)
            return jugador_uno.upper(), jugador2
        else:
            jugador_uno = input("Por favor elija 'X' o 'O' ")


def posicion_actual(tablero, actual, posicion):
    tablero[posicion] = actual
    return tablero


def verificar_posicion(tablero, posicion):
    return tablero[posicion] == '#'  # esto significa que el espacio esta vacio


# Verificar si el tablero ya se llenó


def verificar_tablero(tablero):
    return len([x for x in tablero if x == '#']) == 1


def verificar_jugadas(tablero, posicion_actual):
    if tablero[1] == tablero[2] == tablero[3] == posicion_actual:
        return True
    if tablero[4] == tablero[5] == tablero[6] == posicion_actual:
        return True
    if tablero[7] == tablero[8] == tablero[9] == posicion_actual:
        return True
    if tablero[1] == tablero[4] == tablero[7] == posicion_actual:
        return True
    if tablero[2] == tablero[5] == tablero[8] == posicion_actual:
        return True
    if tablero[3] == tablero[6] == tablero[9] == posicion_actual:
        return True
    if tablero[1] == tablero[5] == tablero[9] == posicion_actual:
        return True
    if tablero[3] == tablero[5] == tablero[7] == posicion_actual:
        return True
    return False


def opcion_jugador(tablero):
    opcion = input("Elija su jugada en los espacios del 1-9 disponibles: ")
    while not verificar_posicion(tablero, int(opcion)):
        opcion = input(
            "La posición ya ha sido jugada. Elija su jugada en los espacios del 1-9 disponibles: ")
    return opcion
